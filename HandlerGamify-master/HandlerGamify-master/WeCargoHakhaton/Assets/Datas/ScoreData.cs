﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Score")]
public class ScoreData : ScriptableObject
{
    public List<float> scores = new List<float>();
}
