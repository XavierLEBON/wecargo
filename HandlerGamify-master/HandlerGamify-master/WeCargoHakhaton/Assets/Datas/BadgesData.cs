﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Badges")]
public class BadgesData : ScriptableObject
{
    public List<bool> badges = new List<bool>();
}
