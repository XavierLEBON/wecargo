﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public ScoreData score;

    public static ScoreManager instance;

    private void Awake()
    {
        if (instance)
            Destroy(this);
        else
            instance = this;
    }

    public void AddScore(float scoreToAdd)
    {
        score.scores.Add(scoreToAdd);
        score.scores.Sort();
        FindObjectOfType<ScoreDisplay>().DisplayScore();

        if(scoreToAdd <= 25)
        {
            BadgesManager.instance.GetBadge(2);
        }
    }
}
