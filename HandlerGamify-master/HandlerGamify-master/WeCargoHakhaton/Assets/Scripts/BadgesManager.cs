﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BadgesManager : MonoBehaviour
{
    public static BadgesManager instance;

    public List<GameObject> badges;

    public BadgesData badgeData;

    public GameObject congratz;
    public Transform congratzPos;
    public Transform frontCanvas;
    public GameObject particle;

    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;
    }

    private void Start()
    {
        UpdateBadges();
        StartCoroutine("FirstBadge");
    }

    public void GetBadge(int badgeNumber)
    {
        try
        {
            NewBadge(badgeNumber);
            UpdateBadges();
        }
        catch
        {
            Debug.Log("There is no such badge...");
        }
    }

    private void NewBadge(int badgeNumber)
    {
        if (!badgeData.badges[badgeNumber])
        {
            badgeData.badges[badgeNumber] = true;
            StartCoroutine("Congratulation");
        }
    }

    private IEnumerator Congratulation()
    {
        GameObject congratulate = Instantiate(congratz, frontCanvas);
        particle.SetActive(true);
        yield return new WaitForSeconds(3);
        Destroy(congratulate);
        particle.SetActive(false);
    }

    public void UpdateBadges()
    {
        for (int i = 0; i < badges.Count; i++)
        {
            if (badgeData.badges[i] == true)
                badges[i].GetComponent<Image>().color = Color.white;
            else
                badges[i].GetComponent<Image>().color = Color.black;

        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < badgeData.badges.Count; i++)
            {
                badgeData.badges[i] = false;
            }
            UpdateBadges();
        }
    }

    private IEnumerator FirstBadge()
    {
        yield return new WaitForSeconds(5);
        GetBadge(0);
    }
}
