﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager instance;
    public Transform inventoryParent;
    public Transform achievedParent;

    [HideInInspector] public List<GameObject> luggages = new List<GameObject>();

    private bool hasAchieved = false;

    void Start()
    {
        if (instance)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    public void AddToInventory(GameObject newItem)
    {
        newItem.transform.SetParent(inventoryParent);
        luggages.Add(newItem);
    }

    public void AddToAchieved(Color achievedColor)
    {
        GameObject achievedObject = luggages[0];
        achievedObject.transform.SetParent(achievedParent);
        luggages.Remove(achievedObject);
        achievedObject.GetComponent<LuggageBehaviour>().isAchieved = true;
        ScoreManager.instance.AddScore(achievedObject.GetComponent<LuggageBehaviour>().timer);

        BadgesManager.instance.GetBadge(1);
        hasAchieved = true;

    }
}
