﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDisplay : MonoBehaviour
{
    private void OnEnable()
    {
        InventoryImerToAdd.instance.OnLuggageDisplay();
    }

    private void OnDisable()
    {
        InventoryImerToAdd.instance.OnLuggageHide();
    }
}
