﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LuggageManager : MonoBehaviour
{
    public static LuggageManager instance;
    public Transform luggageParent;
    public GameObject luggage;

    public List<string> luggageNames;
    public Vector2 luggageLifetime;
    public List<Color> colors;
    public Vector2 luggageSpawnRange;
    private float currentTimer;

    private void Start()
    {
        if (instance)
        {
            Destroy(this);
        } else
        {
            instance = this;
        }

        SetTimer();
    }

    private void Update()
    {
        CountDown();
    }

    private void CountDown()
    {
        currentTimer -= Time.deltaTime;
        if (currentTimer < 0)
        {
            SpawnLuggage();
            SetTimer();
        }
    }

    private void SpawnLuggage()
    {
        GameObject newLuggage = Instantiate(luggage, luggageParent);
        LuggageBehaviour luggageBehaviour = newLuggage.GetComponent<LuggageBehaviour>();
        luggageBehaviour.nameText.text = GetName();
        luggageBehaviour.timer = GetTimer();
        luggageBehaviour.image.color = Random.ColorHSV();
    }

    private float GetTimer()
    {
        float random = Random.Range(luggageLifetime.x, luggageLifetime.y);
        return random;
    }

    private string GetName()
    {
        int random = Random.Range(0, luggageNames.Count);
        return luggageNames[random];
    }

    void SetTimer()
    {
        float newTimer = Random.Range(luggageSpawnRange.x, luggageSpawnRange.y);
        currentTimer = newTimer;
    }
}
