﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    public ScoreData scores;
    public GameObject scoreImage;
    public Transform scoreParent;

    private List<GameObject> scoreList = new List<GameObject>();

    private void Start()
    {
        DisplayScore();
    }

    public void DisplayScore()
    {
        foreach (var score in scoreList)
        {
            Destroy(score);
        }

        foreach (var score in scores.scores)
        {
            GameObject newImage = Instantiate(scoreImage, scoreParent);
            newImage.GetComponentInChildren<Text>().text = Convert.ToInt32(score).ToString();
            scoreList.Add(newImage);
        }
    }
}
