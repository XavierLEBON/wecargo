﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LuggageBehaviour : MonoBehaviour
{
    public Text nameText;
    public Text timerText;
    public Image image;
    [SerializeField] private GameObject button;
    public float timer;

    private bool isTimedOut = false;

    [HideInInspector] public bool isAchieved = false;

    private void Awake()
    {
        LuggageTimerToAdd.instance.onLuggageDisplay += LuggageDisplayHandler;
        InventoryImerToAdd.instance.onInventoryDisplay += InventoryDisplayHandler;
    }


    private void OnDestroy()
    {
        InventoryImerToAdd.instance.onInventoryDisplay += InventoryDisplayHandler;
        LuggageTimerToAdd.instance.onLuggageDisplay -= LuggageDisplayHandler;
    }

    private void InventoryDisplayHandler(float obj)
    {
        if (isTimedOut)
            timer += obj;
    }

    private void LuggageDisplayHandler(float timeToRemove)
    {
        if (!isTimedOut)
            timer -= timeToRemove;
    }

    private void Update()
    {
        if (!isAchieved)
        {
            if (!isTimedOut)
                CountDown();

            if (isTimedOut)
                CountUp();
        }

        DisplayTimer();
    }

    private void CountUp()
    {
        timer += Time.deltaTime;
    }

    private void CountDown()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            timer = 0;
            isTimedOut = true;
            button.SetActive(true);
        }
    }

    private void DisplayTimer()
    {
        int time = (int)timer;

        int minutes = Convert.ToInt32(time / 60);
        int seconds = Convert.ToInt32(time % 60);

        timerText.text = minutes.ToString() + " : " + seconds.ToString();
    }

    public void AddToInventory()
    {
        InventoryManager.instance.AddToInventory(this.gameObject);
        button.SetActive(false);
    }
}
