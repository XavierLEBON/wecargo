﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarehouseBehaviour : MonoBehaviour
{
    public static WarehouseBehaviour instance;

    public List<PaletteBehaviour> boxes = new List<PaletteBehaviour>();
    public List<bool> busyBox = new List<bool>();

    private PaletteBehaviour activePalette;
    private int activePaletteNumber;

    private void Awake()
    {
        if (instance)
            Destroy(this);
        else
            instance = this;
    }

    public void SetActivePalette(int number)
    {
        if (!busyBox[number] == true)
        {
            activePalette = boxes[number];
            activePaletteNumber = number;
        }
        else
            Debug.Log("this box is busy");
    }

    public void ActivateBox(Color boxColor)
    {
        if (!busyBox[activePaletteNumber])
        {
            busyBox[activePaletteNumber] = true;
            activePalette.boxToShow.SetActive(true);
            foreach (var item in activePalette.materials)
            {
                item.material.color = boxColor;
            }
        }
    }
}
