﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarehouseInventoryDisplay : MonoBehaviour
{
    public Transform top;
    public Transform bottom;
    public GameObject item;
    public int maxRowNumber;

    public List<GameObject> topButtons = new List<GameObject>();
    public List<GameObject> bottomButtons = new List<GameObject>();

    private void OnEnable()
    {
        DisplayInventory();
    }

    private void OnDisable()
    {
        foreach (var item in topButtons)
        {
            Destroy(item);
        }

        foreach (var item in bottomButtons)
        {
            Destroy(item);
        }
    }

    private void DisplayInventory()
    {
        foreach (GameObject luggage in InventoryManager.instance.luggages)
        {
            if (topButtons.Count < maxRowNumber)
            {
                InstantiateItem(top, luggage, topButtons);
            }
            else if (bottomButtons.Count < maxRowNumber)
            {
                InstantiateItem(bottom, luggage, bottomButtons);
            }
            else if (bottomButtons.Count >= maxRowNumber)
            {
                Debug.Log("Too Many Items");
            }
        }
    }

    private void InstantiateItem(Transform parent, GameObject luggage, List<GameObject> liste)
    {
        GameObject newItem = Instantiate(item, parent);
        newItem.GetComponent<Image>().color = luggage.GetComponent<LuggageBehaviour>().image.color;
        liste.Add(newItem);
    }

    public void RemoveItem(GameObject itemToRemove)
    {
        if (topButtons.Contains(itemToRemove))
        {
            topButtons.Remove(itemToRemove);
        }
        else if (bottomButtons.Contains(itemToRemove))
        {
            bottomButtons.Remove(itemToRemove);
        }
    }
}
