﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncomingDisplay : MonoBehaviour
{
    private void OnEnable()
    {
        LuggageTimerToAdd.instance.OnLuggageDisplay();
    }

    private void OnDisable()
    {
        LuggageTimerToAdd.instance.OnLuggageHide();
    }
}
