﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuggageTimerToAdd : MonoBehaviour
{
    public static LuggageTimerToAdd instance;

    private float timerToAdd = 0;
    private bool isCounting = true;

    public event Action<float> onLuggageDisplay;

    private void Awake()
    {
        if (instance)
            Destroy(this);
        else
            instance = this;
    }

    private void Update()
    {
        if(isCounting)
        Count();
    }

    private void Count()
    {
        timerToAdd += Time.deltaTime;
    }

    public void OnLuggageHide()
    {
        isCounting = true;
    }

    public void OnLuggageDisplay()
    {
        isCounting = false;
        onLuggageDisplay?.Invoke(timerToAdd);
        timerToAdd = 0;
    }
}
