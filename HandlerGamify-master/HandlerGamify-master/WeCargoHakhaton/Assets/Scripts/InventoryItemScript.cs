﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemScript : MonoBehaviour
{
    public void Activate()
    {
        WarehouseBehaviour.instance.ActivateBox(GetComponent<Image>().color);
    }

    public void Deactivate()
    {
        FindObjectOfType<WarehouseInventoryDisplay>().RemoveItem(this.gameObject);
        InventoryManager.instance.AddToAchieved(GetComponent<Image>().color);
        Destroy(this.gameObject);
    }
}
